FROM ubuntu

ARG NODE_VERSION=0

ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt upgrade -y
RUN apt install -y unzip curl git software-properties-common wget sudo software-properties-common rsync supervisor cron gpg gpg-agent --no-install-recommends

# Node
RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash -
RUN apt-get install -y nodejs --no-install-recommends
RUN npm install -g create-ts-index yarn

# Tools
RUN add-apt-repository ppa:ondrej/php && apt update
RUN apt install -y php7.4-cli php7.4-xml  --no-install-recommends

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"
ENV COMPOSER_HOME="/usr/local/composer" 
RUN composer global require fluxter/fxrelease

RUN echo 'PATH="$PATH:/usr/local/composer/vendor/bin"' >> /etc/profile

# Swagger codegen
RUN wget https://repo1.maven.org/maven2/io/swagger/swagger-codegen-cli/2.4.15/swagger-codegen-cli-2.4.15.jar -O /opt/swagger-codegen-cli.jar \
	&& echo '#!/bin/bash\njava -jar /opt/swagger-codegen-cli.jar $@' > /usr/bin/swagger-codegen \
	&& chmod +x /usr/bin/swagger-codegen
	
# Openapi Codegen
RUN wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/5.0.0-beta/openapi-generator-cli-5.0.0-beta.jar -O /opt/openapi-generator.jar \
	&& echo '#!/bin/bash\njava -jar /opt/openapi-generator.jar $@' > /usr/bin/openapi-generator \
	&& chmod +x /usr/bin/openapi-generator

# # Cleanup
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*
